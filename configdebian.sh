#!/bin/bash

# Redirigir salida a un log
exec > >(tee -i /var/log/configdebian.log)
exec 2>&1

# Definir colores
GREEN="\e[32m"
RED="\e[31m"
BLUE="\e[34m"
NC="\e[0m" # No Color

# Función para confirmar con "s/n"
function preguntar_si_no() {
    while true; do
        echo -e "${BLUE}$1 (s/n):${NC}"
        read -p "" sn
        case $sn in
            [Ss]* ) return 0 ;;
            [Nn]* ) return 1 ;;
            * ) echo "Por favor, pulsa s o n." ;;
        esac
    done
}

# Función para verificar un comando y actualizar el PATH si es necesario
function verificar_comando() {
    local comando=$1
    if ! command -v "$comando" &> /dev/null; then
        echo "El comando $comando no está en el PATH. Actualizando PATH..."
        export PATH="$PATH:/sbin:/usr/sbin"
        if command -v "$comando" &> /dev/null; then
            echo -e "${GREEN}PATH actualizado correctamente. $comando está disponible.${NC}"
        else
            echo -e "${RED}El comando $comando sigue sin estar disponible. Asegúrate de que esté instalado.${NC}"
        fi
    fi
}

# Función para instalar swap
function instalar_swap() {
    echo -e "${BLUE}Configurando una partición SWAP de 2GB...${NC}"

    # Crear el archivo swap de 2GB
    if dd if=/dev/zero of=/swapfile bs=1M count=2048 status=progress; then
        echo -e "${GREEN}Archivo swap de 2GB creado correctamente.${NC}"
    else
        echo -e "${RED}Error al crear el archivo swap.${NC}"
        exit 1
    fi

    # Ajustar permisos
    chmod 600 /swapfile
    echo -e "${GREEN}Permisos del archivo swap configurados.${NC}"

    # Configurar el swap
    if mkswap /swapfile; then
        echo -e "${GREEN}Archivo swap inicializado.${NC}"
    else
        echo -e "${RED}Error al inicializar el archivo swap.${NC}"
        exit 1
    fi

    # Activar el swap
    if swapon /swapfile; then
        echo -e "${GREEN}Archivo swap activado correctamente.${NC}"
    else
        echo -e "${RED}Error al activar el archivo swap.${NC}"
        exit 1
    fi

    # Añadir a /etc/fstab para que sea permanente
    if ! grep -q "/swapfile" /etc/fstab; then
        echo '/swapfile none swap sw 0 0' >> /etc/fstab
        echo -e "${GREEN}Swap añadido a /etc/fstab.${NC}"
    else
        echo -e "${BLUE}El archivo swap ya está en /etc/fstab.${NC}"
    fi
}

# Función para instalar paquetes comunes
function instalar_paquetes_comunes() {
    apt install -y linux-headers-$(uname -r|sed 's/[^-]*-[^-]*-//') build-essential make automake cmake autoconf git aptitude curl gparted locate
}

# Función para instalar Flatpak
function instalar_flatpak() {
    echo "Instalando Flatpak..."
    apt install -y flatpak

    case $ENTORNO in
        gnome | x-cinnamon)
            apt install -y gnome-software-plugin-flatpak
            ;;
        kde)
            apt install -y plasma-discover-backend-flatpak
            ;;
    esac

    # Añadir Flathub para flatpak
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
}

# Función para instalar Snap
function instalar_snap() {
    echo "Instalando Snap..."
    apt install -y snapd

    # Verificar si snapd está activo, si no lo está, habilitarlo
    systemctl is-active --quiet snapd || systemctl start snapd
    systemctl enable snapd
}

# Función para verificar el entorno gráfico
ENTORNO=$(echo "$XDG_CURRENT_DESKTOP" | tr '[:upper:]' '[:lower:]')

# Mostrar cabecera del script
echo
echo "************************************************************************"
echo "**                           CONFIGDEBIAN                             **"
echo "************************************************************************"
echo
echo "Este es el script de Roberto Díaz, para configurar tu Debian 12."
sleep 2s

# Detectar el usuario no root
USER=$(logname 2>/dev/null || echo "$SUDO_USER")

# Si no se pudo detectar automáticamente, pedir que lo ingrese
if [ -z "$USER" ]; then
    echo -e "${BLUE}Introduce el nombre del usuario no root que deseas añadir a sudoers:${NC}"
    read -p "" USER
fi

# Modificar sudoers y los repositorios
echo "$USER ALL=(ALL) ALL" >> /etc/sudoers

echo "Comentando repositorios del DVD en /etc/apt/sources.list..."
sed -i 's/^deb cdrom/# deb cdrom/' /etc/apt/sources.list

# Actualizar /etc/apt/sources.list con non-free-firmware
cat > /etc/apt/sources.list <<EOL
#------------------------------------------------------------------------------#
#                            OFFICIAL DEBIAN REPOS                             #
#------------------------------------------------------------------------------#
                          ###### Debian Main Repos ######

# Repositorios principales de Debian
deb http://deb.debian.org/debian/ bookworm main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian/ bookworm main contrib non-free non-free-firmware

# Actualizaciones de seguridad
deb http://security.debian.org/debian-security/ bookworm-security main contrib non-free non-free-firmware
deb-src http://security.debian.org/debian-security/ bookworm-security main contrib non-free non-free-firmware

# Actualizaciones regulares
deb http://deb.debian.org/debian/ bookworm-updates main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian/ bookworm-updates main contrib non-free non-free-firmware

# Repositorios backports
deb http://deb.debian.org/debian/ bookworm-backports main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian/ bookworm-backports main contrib non-free non-free-firmware
EOL

echo "Actualizando sistema..."
apt update && apt upgrade -y && apt autoremove -y && apt autoclean -y && apt clean -y

# Preguntar para crear swapfile
if preguntar_si_no "¿Quieres crear un archivo swap?"; then
    instalar_swap
fi

# Instalar firmware
echo "Instalando firmware..."
apt install -y firmware-linux firmware-linux-free firmware-linux-nonfree firmware-misc-nonfree
echo -e "${GREEN}Firmware instalado correctamente.${NC}"

# Instalar drivers privativos de NVIDIA
if preguntar_si_no "¿Quieres instalar el driver privativo de NVIDIA?"; then
    apt install -y nvidia-detect
    driver=$(nvidia-detect | grep -o 'nvidia-[^ ]*')
    apt install -y "$driver"
fi

# Instalar Flatpak
if preguntar_si_no "¿Quieres instalar Flatpak?"; then
    instalar_flatpak
fi

# Instalar Snap
if preguntar_si_no "¿Quieres instalar Snap?"; then
    instalar_snap
fi

# Instalar aplicaciones adicionales
if preguntar_si_no "¿Quieres instalar algunas aplicaciones?"; then
    case $ENTORNO in
        gnome)
            apt install -y nautilus-share gdebi chrome-gnome-shell inxi
            ;;
        kde)
            apt install -y synaptic ffmpegthumbs
            ;;
    esac
    instalar_paquetes_comunes
fi

# Instalar navegadores extra
if preguntar_si_no "¿Quieres instalar algún navegador extra como Chrome, Brave...etc?"; then
    (./navegadores.sh)
fi

# Eliminar juegos de GNOME
if [ "$ENTORNO" = "gnome" ]; then
    if preguntar_si_no "¿Quieres eliminar los juegos de GNOME?"; then
        apt purge -y gnome-games && apt autoremove -y gnome-games
    fi
fi

# Verificar y actualizar el PATH si reboot y update-grub no están disponibles
verificar_comando reboot
verificar_comando update-grub

# Actualizar grub
echo "Actualizando GRUB..."
update-grub

# Reiniciar sistema
if preguntar_si_no "¿Quieres reiniciar el equipo?"; then
    reboot
fi

echo -e "${GREEN}¡Debian está listo!${NC}"