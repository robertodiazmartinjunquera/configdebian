# CONFIGDEBIAN

<p>Esto es un script para configurar Debian cuando lo instalamos desde 0, el script esta destinado sobretodo a esas personas que se hacercan a debian por primera vez y no saben como configurarlo correctamente o instalar sus drivers...

Este script esta pensado unicamente para Debian 11 Bullseye y funciona con cualquier entorno de escritorio ya sea GNOME, KDE PLASMA, XFCE...</p>


# FUNCIONAMIENTO/INSTALACIÓN:

El script debe ejecutarse como usuario root y con permisos de Ejecución para poder funcionar correctamente, después simplemente ejecute el script.
Por favor asegurese de descargar los archivos configdebian.sh y navegadores.sh en el mismo directorio para que el script pueda funcionar correctamente.

Las ordenes necesarias para ejecutar el script son las siguientes:

```bash
su                          #para entrar como usuario root
```

```bash
chmod +x *.sh    #para dar permisos de ejecución al script
```

```bash
./configdebian.sh          #Para ejecutar el script
```

## Clonación del Repositorio

Para clonar este repositorio, asegúrate de tener instalado Git en tu sistema. Puedes clonar el repositorio usando el siguiente comando:

```bash
git clone https://gitlab.com/robertodiazmartinjunquera/configdebian.git