#!/bin/bash

# Definir colores
GREEN="\e[32m"
RED="\e[31m"
BLUE="\e[34m"
NC="\e[0m" # Sin color

# Menú de navegadores
while true; do
    echo -e "${BLUE}Indica qué navegador deseas instalar:${NC}"
    sleep 1s
    echo
    echo "  1: Última versión de Firefox (Rama estable)"
    echo "  2: Google Chrome"
    echo "  3: Brave Browser"
    echo "  4: Microsoft Edge"
    echo "  5: Chromium"
    echo "  6: Opera"
    echo "  7: Vivaldi"
    echo "  8: Tor Browser"
    echo "  n: Cancelar"
    echo
    sleep 1s
    read -p "Opción: " sn
    echo

    case $sn in
        1)
            wget "https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=es-ES" -O latest-firefox.tar.bz2
            rm -rf /opt/firefox
            tar -jxvf latest-firefox.tar.bz2 -C /opt
            ln -sf /opt/firefox/firefox /usr/bin/firefox
            cat <<EOF > /usr/share/applications/firefox.desktop
[Desktop Entry]
Name=Firefox
Comment=Navegador web
GenericName=Web Browser
Exec=/opt/firefox/firefox %u
Terminal=false
Type=Application
Icon=/opt/firefox/browser/chrome/icons/default/default128.png
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;
StartupWMClass=Firefox
StartupNotify=true
EOF
            rm latest-firefox.tar.bz2
            echo -e "${GREEN}Firefox instalado correctamente.${NC}"
            ;;
        2)
            echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list
            wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
            apt update -y && apt install -y google-chrome-stable
            echo -e "${GREEN}Google Chrome instalado correctamente.${NC}"
            ;;
        3)
            apt install -y apt-transport-https curl gnupg
            curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | apt-key add -
            echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" > /etc/apt/sources.list.d/brave-browser-release.list
            apt update && apt install -y brave-browser
            echo -e "${GREEN}Brave Browser instalado correctamente.${NC}"
            ;;
        4)
            apt install -y apt-transport-https ca-certificates curl software-properties-common
            wget -q -O - https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /usr/share/keyrings/microsoft-edge.gpg
            echo "deb [arch=amd64 signed-by=/usr/share/keyrings/microsoft-edge.gpg] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge.list
            apt update && apt install -y microsoft-edge-stable
            echo -e "${GREEN}Microsoft Edge instalado correctamente.${NC}"
            ;;
        5)
            apt install -y chromium
            echo -e "${GREEN}Chromium instalado correctamente.${NC}"
            ;;
        6)
            add-apt-repository -y 'deb https://deb.opera.com/opera-stable/ stable non-free'
            wget -qO- https://deb.opera.com/archive.key | apt-key add -
            apt update && apt install -y opera-stable
            echo -e "${GREEN}Opera instalado correctamente.${NC}"
            ;;
        7)
            wget -qO- https://repo.vivaldi.com/archive/linux_signing_key.pub | gpg --dearmor > /usr/share/keyrings/vivaldi-browser.gpg
            echo "deb [signed-by=/usr/share/keyrings/vivaldi-browser.gpg arch=$(dpkg --print-architecture)] https://repo.vivaldi.com/archive/deb/ stable main" > /etc/apt/sources.list.d/vivaldi-archive.list
            apt update && apt install -y vivaldi-stable
            echo -e "${GREEN}Vivaldi instalado correctamente.${NC}"
            ;;
        8)
            apt install -y torbrowser-launcher
            echo -e "${GREEN}Tor Browser instalado correctamente.${NC}"
            ;;
        [Nn])
            echo -e "${RED}Cancelando instalación de navegadores.${NC}"
            exit 0
            ;;
        *)
            echo -e "${RED}Opción inválida. Por favor, selecciona una opción válida.${NC}"
            ;;
    esac

    # Preguntar si desea instalar otro navegador
    echo
    read -p "¿Deseas instalar otro navegador? (s/n): " continue_choice
    case $continue_choice in
        [Nn]*) 
            echo -e "${RED}Saliendo del menú de navegadores.${NC}"
            exit 0
            ;;
        [Ss]*) 
            echo -e "${BLUE}Volviendo al menú de selección...${NC}"
            ;;
        *) 
            echo -e "${RED}Respuesta no válida. Saliendo del menú de navegadores.${NC}"
            exit 0
            ;;
    esac
done